package utils;

import java.util.ArrayList;

import com.db.Initiate;
import com.db.RDSControl;

import org.kie.api.runtime.process.ProcessContext;

/**
 * Scripts
 */
public class Scripts {

    public static void initiateControl(ProcessContext kcontext) {
        ArrayList<RDSControl> control = new ArrayList<>();
        
        control.add(new RDSControl("group1", "group1","group1","1m","2m","3m"));
        control.add(new RDSControl("group2", "group2","group2","1m","2m","3m"));
        control.add(new RDSControl("group1", "group2","group1","1m","2m","3m"));
        kcontext.setVariable("control", control);

        Initiate initiate = new Initiate();
        initiate.setControl(control);
        kcontext.setVariable("initiate", initiate);
    }

    public static void initiateOnExit(ProcessContext kcontext) {
        Initiate initiate = (Initiate) kcontext.getVariable("initiate");
        kcontext.setVariable("control", initiate.getControl());
    }

    public static void addDetailsOnEntry(ProcessContext kcontext) {
        System.out.println(kcontext.getVariable("ci"));
    }
}