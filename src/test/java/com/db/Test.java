package com.db;

import java.util.List;
import java.util.Map;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.dmn.api.core.DMNContext;
import org.kie.dmn.api.core.DMNDecisionResult;
import org.kie.dmn.api.core.DMNModel;
import org.kie.dmn.api.core.DMNResult;
import org.kie.dmn.api.core.DMNRuntime;

/**
 * Test
 */
public class Test {

    public static void main(String[] args) {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();

        // KieRuntimeFactory rf = KieRuntimeFactory.of(kieContainer.getKieBase());
        // DMNRuntime dmnRuntime = rf.get(DMNRuntime.class);

        DMNRuntime dmnRuntime = kieContainer.newKieSession()
                    .getKieRuntime(DMNRuntime.class);

        DMNModel model = dmnRuntime.getModel("https://kiegroup.org/dmn/_9DB8EDDC-36C8-4469-8D57-EEED57D0CA2B",
                "CaseContext");

        DMNContext context = dmnRuntime.newContext();
        context.set("sequence", 2);

        DMNResult result = dmnRuntime.evaluateByName(model, context, "CaseContext");

        DMNDecisionResult decisionResult = result.getDecisionResultByName("CaseContext");
        System.out.println(decisionResult.getResult());
        //decisionResult.getResult()
        Object resultObj = decisionResult.getResult();
        if (resultObj instanceof List) {
            List<?> list = (List<?>) resultObj;
            for (Object object : list) {
                if (object instanceof Map<?,?>) {
                    Map<?,?> map = (Map<?,?>) object;
                    System.out.println(map);
                }
            }
        } else if (resultObj instanceof Map<?,?>) {
            Map<?,?> map = (Map<?,?>) resultObj;
            System.out.println(map);
        }

    }

}