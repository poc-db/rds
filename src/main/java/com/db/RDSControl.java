package com.db;

import java.io.Serializable;

public class RDSControl implements Serializable {

	static final long serialVersionUID = 1L;

	private String enrichGroup;
	private String approveGroup;
	private String updateGroup;
	private String enrichSLA;
	private String approveSLA;
	private String updateSLA;
	private String details;

	public RDSControl() {
	}

	public RDSControl(String enrichGroup, String approveGroup, String updateGroup, String enrichSLA, String approveSLA,
			String updateSLA) {
		this.enrichGroup = enrichGroup;
		this.approveGroup = approveGroup;
		this.updateGroup = updateGroup;
		this.enrichSLA = enrichSLA;
		this.approveSLA = approveSLA;
		this.updateSLA = updateSLA;
	}

	@Override
	public String toString() {
		return String.format(
				"RDSControl {approveGroup: %s, approveSLA: %s, enrichGroup: %s, enrichSLA: %s, updateGroup: %s, updateSLA: %s }",
				approveGroup, approveSLA, enrichGroup, enrichSLA, updateGroup, updateSLA);
	}

	public String getEnrichGroup() {
		return this.enrichGroup;
	}

	public void setEnrichGroup(String enrichGroup) {
		this.enrichGroup = enrichGroup;
	}

	/**
	 * @return the approveGroup
	 */
	public String getApproveGroup() {
		return approveGroup;
	}

	/**
	 * @param approveGroup the approveGroup to set
	 */
	public void setApproveGroup(String approveGroup) {
		this.approveGroup = approveGroup;
	}

	/**
	 * @return the updateGroup
	 */
	public String getUpdateGroup() {
		return updateGroup;
	}

	/**
	 * @param updateGroup the updateGroup to set
	 */
	public void setUpdateGroup(String updateGroup) {
		this.updateGroup = updateGroup;
	}

	/**
	 * @return the enrichSLA
	 */
	public String getEnrichSLA() {
		return enrichSLA;
	}

	/**
	 * @param enrichSLA the enrichSLA to set
	 */
	public void setEnrichSLA(String enrichSLA) {
		this.enrichSLA = enrichSLA;
	}

	/**
	 * @return the approveSLA
	 */
	public String getApproveSLA() {
		return approveSLA;
	}

	/**
	 * @param approveSLA the approveSLA to set
	 */
	public void setApproveSLA(String approveSLA) {
		this.approveSLA = approveSLA;
	}

	/**
	 * @return the updateSLA
	 */
	public String getUpdateSLA() {
		return updateSLA;
	}

	/**
	 * @param updateSLA the updateSLA to set
	 */
	public void setUpdateSLA(String updateSLA) {
		this.updateSLA = updateSLA;
	}

	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(String details) {
		this.details = details;
	}

}