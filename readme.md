Dynamic Process Definition Proof of Concept
===========================================

This project shows how to design a process definition in a dynamic fashion.

The BPMN diagram contains a set of process fragments:

![RDS-case](src/main/resources/RDS.RDS-case-svg.svg)

When and how to trigger the process fragments are defined in a decision table inside the **Case Context** DMN:

![decision table](docs/case-context-decision-table.png)

The input for the decision is a sequence number and the output is a case context, that specifies the name of the process fragment, the potential owner group and the SLA expression (e.g. `1d` for a day).

It is worth noting that the decision table outcome is a list of case context: this means that for a given sequence number more fragments can be triggered.

The BPMN diagram is like a palette of possible task patterns and the decision table is a way to ensemble together the palette elements.

The controller logic is in the class `CaseScript`, the script nodes *start* and *next* call the class methods with the same names. The logic is:

1. Evaluate DMN with the current value of `sequence` process variable
2. Use the DMN results to trigger the process fragments (variable `group` and `sla` are passed to the engine to initialize the tasks)
3. increase the `sequence` value

4 eyes approval
-------------------------------------------

Another interesting proof point is the 4 eyes approval logic in the `enrich` fragment:

- the actual user of the first task `add details` is copied in the `lastActor` variable
- the following `approve` task, has the *special* parameter `ExcludedOwnerId` initialized to `lastActor`

Start the process
-------------------------------------------

In RHPAM, a process with those *advanced dynamic* features is even called **case**: from a process engine point of view, it is a process with a dedicated runtime context that has to be initialized when dealing with those special process instances.

The engine exposes a set of dedicated APIs to start a case instance and handle the advanced features.

One of the side effect is that it's not possible to start a case with the same process API to start the standard process; moreover, Business Central does not offer a way launch a case instance.

RHPAM offers an optional UI designed to interact with cases and to showcase their key features: to enable it, you have to install the *case management showcase application* (rhpam-7.6.0-case-mgmt-showcase-eap7-deployable.zip inside add-ons package). 

For the needs of this proof of concept, another way to launch a case instance is using the REST APIs:

```sh
curl -u <user>:<password> -X POST "http://localhost:8080/kie-server/services/rest/server/containers/RDS_1.0.0-SNAPSHOT/cases/RDS.RDS-case/instances" -H "accept: application/json" -H "content-type: application/json" -d "{}"
```

