package utils;

import java.util.List;
import java.util.Map;

import org.kie.api.runtime.KieRuntimeFactory;
import org.kie.api.runtime.process.ProcessContext;
import org.kie.dmn.api.core.DMNContext;
import org.kie.dmn.api.core.DMNDecisionResult;
import org.kie.dmn.api.core.DMNModel;
import org.kie.dmn.api.core.DMNResult;
import org.kie.dmn.api.core.DMNRuntime;

/**
 * CaseScript
 */
public class CaseScripts {

    private static DMNRuntime dmnRuntime = null;

    public static void start(ProcessContext kcontext) {
        Long sequence = 1L;

        evaluateCaseContext(kcontext, sequence);
    }

    public static void next(ProcessContext kcontext) {
        Long sequence = (Long) kcontext.getVariable("sequence");

        evaluateCaseContext(kcontext, sequence);
    }

    private static void evaluateCaseContext(ProcessContext kcontext, Long sequence) {
        initDMNRuntime(kcontext);

        Object caseContext = getCaseContext(sequence);
        // TODO: remove
        System.out.println(caseContext);

        // if a next step is defined
        if (caseContext != null) {

            // list of case context
            if (caseContext instanceof List) {
                List<?> list = (List<?>) caseContext;
                for (Object object : list) {
                    if (object instanceof Map<?, ?>) {
                        Map<?, ?> map = (Map<?, ?>) object;
                        triggerNextFragment(kcontext, map);
                    }
                }
                // list of case context
            } else if (caseContext instanceof Map<?, ?>) {
                Map<?, ?> map = (Map<?, ?>) caseContext;
                triggerNextFragment(kcontext, map);
            }

            kcontext.setVariable("sequence", sequence + 1);
        }
    }

    private static void triggerNextFragment(ProcessContext kcontext, Map<?, ?> map) {
        kcontext.setVariable("sla", map.get("sla"));

        kcontext.getProcessInstance()
                .signalEvent((String) map.get("fragment"), map);
    }

    private static Object getCaseContext(Long sequence) {
        DMNModel model = dmnRuntime.getModel("https://kiegroup.org/dmn/_9DB8EDDC-36C8-4469-8D57-EEED57D0CA2B",
                "CaseContext");

        DMNContext context = dmnRuntime.newContext();
        context.set("sequence", sequence);

        DMNResult result = dmnRuntime.evaluateByName(model, context, "CaseContext");

        DMNDecisionResult decisionResult = result.getDecisionResultByName("CaseContext");
        return decisionResult.getResult();
    }

    private static void initDMNRuntime(ProcessContext kcontext) {
        if (dmnRuntime == null) {
            KieRuntimeFactory rf = KieRuntimeFactory.of(kcontext.getKieRuntime()
                                                                .getKieBase());
            dmnRuntime = rf.get(DMNRuntime.class);
        }
    }

    public static void debug(ProcessContext kcontext) {
        System.out.println(">>> " + kcontext.getVariable("sla"));
    }
}